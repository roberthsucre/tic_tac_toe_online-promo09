module Preliminaries

  def game_set_up
    display_menu
    sort_choice
  end

  def display_menu
    until [1,2,3].include?( @choice )
      print "\n\n1. NEW GAME\n\n2. JOIN GAME\n\n3. EXIT\n\n"
      @choice = gets.chomp.to_i
    end
  end

  def sort_choice
    case @choice
    when 1
      create_game
      @choice = nil
      game_set_up
    when 2
      join_game
    when 3
      exit
    end
  end

  def create_game
    waiting_message
    get_game_code
    system('clear')
    print "Game created!\nCode: #{ @code }\nPress ENTER to continue..."; gets
  end

  def waiting_message
    system('clear')
    print "\n\nCreating new game...\n\n"
  end

  def get_game_code
    response = RestClient.post( "tic-tac-toe.devtohack.com/games", {} )
    json = JSON.parse( response.body )
    @code = json["code"]
  end

  def join_game
    print "\n\nGame code: "
    @code = gets.chomp.to_i
    @player = Player.new( @code )
    status = @player.join
    if status == 'waiting' then @player.token = 'X'
    elsif status == 'ready' then @player.token = 'O' end
  end

end
