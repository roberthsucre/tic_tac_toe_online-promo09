require 'rest-client'
require 'json'

class Board
  attr_accessor :board

  def initialize
    @board = []
    9.times { |n| @board[n] = "[   ]" }
  end

  def render
    system("clear")
    @board.each_with_index do |item, index|
      print " #{ item } "
      print "\n\n" if [2,5,8].include?( index )
    end
  end

  def full?
    !( @board.flatten.include?("[   ]")  )
  end

  def free?( location )
    return true if @board[ location ] == "[   ]"
    return false
  end

  def update( token, location )
    @board[ location ] = "[ #{ token.to_s } ]"
  end

  def check_vertical?( current_player )
    board = @board.each_slice(3).to_a
    board.transpose.each { |row| return true if row.all? { |cell| cell == "[ #{ current_player.token } ]" } }
    return false
  end

  def check_horizontal?( current_player )
    board = @board.each_slice(3).to_a
    board.each { |row| return true if row.all? { |cell| cell == "[ #{ current_player.token } ]" } }
    return false
  end

  def check_diagonal?( current_player )
    token = "[ #{ current_player.token } ]"
    return true if @board[0] == token && @board[4] == token && @board[8] == token
    return true if @board[2] == token && @board[4] == token && @board[6] == token
    return false
  end
end
