require_relative 'player.rb'
require_relative 'board.rb'
require_relative 'preliminaries.rb'
require 'rest-client'
require 'json'
system("clear")

class Game
  attr_reader :code
  attr_accessor :board, :game_over, :status, :choice, :player, :token, :winner, :current_player
  include Preliminaries

  def initialize
    @game_over = false
    @board = Board.new
  end

  def player_input
    location = [-1]
    loop do
      location = @player.choice
      break if @board.free?( location )
    end
    location
  end

  def check_board_status
    if @board.check_vertical?( @player ) || @board.check_horizontal?( @player ) ||  @board.check_diagonal?( @player )
      @game_over = true
      @winner = @player.code
    elsif @board.full? then @game_over = true end
  end

  def final_message
    puts "\nGame over. You won!\n" if @winner
    puts "\nGame over. You lost\n" unless @winner
  end

  def waiting_round
    puts "\nWaiting for next player to make a move..."
  end

  def local_move
    @player.position = player_input
    @board.update( @player.token, @player.position )
  end

  def active_round
    local_move
    @board.render
    check_board_status
    @board.board = @player.move( @board.board, @game_over )
  end

  def check_status
    response = RestClient.get( "tic-tac-toe.devtohack.com/games/#{ @code }" )
    json = JSON.parse( response.body )
    @status = json['status']
    @current_player = json['current_player']
    if json['board'] then @board.board = json['board'] end
  end

  def execute
    game_set_up
    until @game_over
      check_status
      @board.render
      if @status == 'ready' && @current_player == @player.code
        active_round
      else
        waiting_round
        break if @status == 'finished'
      end
    end
    final_message
  end
end
