require 'rest-client'
require 'json'

class Player
  attr_reader  :game_code
  attr_accessor :position, :token, :code

  def initialize( game_code )
    @game_code = game_code
    @code = rand( 1000..9000 )
  end

  def join
    url = "tic-tac-toe.devtohack.com/games/#{ @game_code }/join"
    response = RestClient.post( url, { player: @code } )
    json = JSON.parse( response.body )
    json['status']
  end

  def move( board, game_over )
    url = "tic-tac-toe.devtohack.com/games/#{ @game_code }"
    response = RestClient.put( url, { player: @code , position: @position.to_s, board: board, game_over: game_over } )
    json = JSON.parse( response.body )
    json['board']
  end

  def choice
    print "\nIt is your turn.. Your token is  #{ @token }\n\n"
    location = -1
    until [0,1,2,3,4,5,6,7,8].include?( location )
      print "Value, please: "
      location = gets.chomp.to_i
    end
    location
  end
end
